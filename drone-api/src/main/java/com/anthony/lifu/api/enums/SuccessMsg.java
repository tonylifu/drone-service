package com.anthony.lifu.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum SuccessMsg {
    SUCCESS("00", "Successful"),
    DRONE_REGISTRATION_SUCCESS("00", "drone was successful registered");

    private final String code;
    private final String description;

    @Override
    public String toString() {
        return code + ": " + description;
    }
}



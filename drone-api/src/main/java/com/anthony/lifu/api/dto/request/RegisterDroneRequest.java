package com.anthony.lifu.api.dto.request;

import com.anthony.lifu.api.enums.DroneModelStatus;
import com.anthony.lifu.api.enums.DroneStateStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDroneRequest {
    private String serialNumber;
    private String model;
    private double weight;
    private Integer battery;
    private String state;
    private Set<AddMedicationRequest> medications;
}

package com.anthony.lifu.api.constants;

import com.anthony.lifu.api.dto.response.ApiResponse;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class AppConstants {

    private static NumberFormat percent = NumberFormat.getPercentInstance();

    public static String getPercentageFromNumber(Double number) {
        percent = new DecimalFormat("0.0#%");
        return percent.format(number/100);
    }

    public static Double getNumberFromPercentage(String numberInPercent) {
        return Double.parseDouble(numberInPercent.substring(0, numberInPercent.length()-2)) * 100;
    }

    public static ApiResponse apiResponse(boolean isSuccess, String serialNumber, String code, String message) {
        return ApiResponse.builder()
                .isSuccess(isSuccess)
                .serialNumber(serialNumber)
                .statusCode(code)
                .statusMessage(message)
                .build();
    }
}

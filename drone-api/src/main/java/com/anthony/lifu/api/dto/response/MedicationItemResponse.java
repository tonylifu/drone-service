package com.anthony.lifu.api.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MedicationItemResponse {
    private String name;
    private double weight;
    private String code;
    private String image;
}

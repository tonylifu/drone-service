package com.anthony.lifu.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddMedicationRequest {
    private String name;
    private double weight;
    private String code;
    private String image;
}

package com.anthony.lifu.api.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiResponse {
    private boolean isSuccess;
    private String serialNumber;
    private String statusCode;
    private String statusMessage;
}

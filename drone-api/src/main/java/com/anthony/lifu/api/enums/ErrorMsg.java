package com.anthony.lifu.api.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ErrorMsg {
    DRONE_NOT_FOUND("01", "drone does not exist"),
    BATTERY_LEVEL_LOW("02", "battery level is too low"),
    DRONE_MAX_WEIGHT_EXCEEDED("03", "drone maximum weight exceeded"),
    MEDICATIONS_NOT_FOUND("04", "no medication found on drone"),
    DRONE_STATE_NOT_FOUND("05", "only [IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING] accepted for drone STATE"),
    DRONE_MODEL_NOT_FOUND("06", "only [LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT] accepted for drone Model"),

    SOMETHING_WENT_WRONG("98", "Something went wrong"),
    FAILED("99", "Failed");

    private final String code;
    private final String description;

    @Override
    public String toString() {
        return code + ": " + description;
    }
}

package com.anthony.lifu.api.enums;

import java.util.HashMap;
import java.util.Map;

public enum DroneModelStatus {
    LIGHTWEIGHT("Lightweight".toLowerCase()),
    MIDDLEWEIGHT("Middleweight".toLowerCase()),
    CRUISERWEIGHT("Cruiserweight".toLowerCase()),
    HEAVYWEIGHT("Heavyweight".toLowerCase()),
    UNKNOWN("Unknown".toLowerCase());

    String status;

    DroneModelStatus(String status) {
        this.status = status;
    }

    private static final Map<String, DroneModelStatus> DRONE_MODEL_MAP = new HashMap<>();

    static {
        for(DroneModelStatus droneModel : values()){
            DRONE_MODEL_MAP.put(droneModel.status, droneModel);
        }
    }

    public static DroneModelStatus fromStringValue(String value){
        return (!DRONE_MODEL_MAP.containsKey(value.trim().toLowerCase()) ||
                value.trim().toLowerCase().equals(UNKNOWN.status)) ?
                UNKNOWN : DRONE_MODEL_MAP.get(value.trim().toLowerCase());
    }
}

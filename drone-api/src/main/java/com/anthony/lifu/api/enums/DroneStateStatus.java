package com.anthony.lifu.api.enums;

import java.util.HashMap;
import java.util.Map;

public enum DroneStateStatus {
    IDLE("IDLE".toLowerCase()),
    LOADING("LOADING".toLowerCase()),
    LOADED("LOADED".toLowerCase()),
    DELIVERING("DELIVERING".toLowerCase()),
    DELIVERED("DELIVERED".toLowerCase()),
    RETURNING("RETURNING".toLowerCase()),
    UNKNOWN("UNKNOWN".toLowerCase());

    String status;

    DroneStateStatus(String status) {
        this.status = status;
    }

    private static final Map<String, DroneStateStatus> BATCH_STATUS_MAP = new HashMap<>();

    static {
        for(DroneStateStatus droneState : values()){
            BATCH_STATUS_MAP.put(droneState.status, droneState);
        }
    }

    public static DroneStateStatus fromStringValue(String value){
        return (!BATCH_STATUS_MAP.containsKey(value.trim().toLowerCase()) ||
                value.trim().toLowerCase().equals(UNKNOWN.status)) ?
                UNKNOWN : BATCH_STATUS_MAP.get(value.trim().toLowerCase());
    }
}

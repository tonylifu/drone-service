package com.anthony.lifu.api.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class MedicationResponse {
    List<MedicationItemResponse> medications;
}

package com.anthony.lifu.model.repository;

import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.model.entity.Drone;
import io.micronaut.context.annotation.Parameter;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

    Optional<Drone> findBySerialNumber(@NotNull String serialNumber);

    List<Drone> findByState(@NotNull DroneStateStatus droneState);

    @Query(value = "select c from Drone c where c.state = :state1 or c.state = :state2")
    List<Drone> getByStateStatuses(@Parameter("state1") DroneStateStatus state1, @Parameter("state2") DroneStateStatus state2);
}

package com.anthony.lifu.model.repository;

import com.anthony.lifu.model.entity.Medication;
import io.micronaut.data.annotation.Repository;
import io.micronaut.data.jpa.repository.JpaRepository;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {
}

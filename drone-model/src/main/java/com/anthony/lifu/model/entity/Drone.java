package com.anthony.lifu.model.entity;

import com.anthony.lifu.api.enums.DroneModelStatus;
import com.anthony.lifu.api.enums.DroneStateStatus;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.data.annotation.DateCreated;
import io.micronaut.data.annotation.DateUpdated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @NonNull
//    @Column(length = 5)
    @Size(min = 1, max = 100)
    private String serialNumber;
    @NonNull
    @Enumerated(EnumType.STRING)
    private DroneModelStatus model;
    @NonNull
    @Digits(integer=5, fraction=2)
    @Column(name = "weight")
    @Max(500)
    private BigDecimal weight;
    @NonNull
    @Min(0) @Max(100)
    private Integer battery;
    @NonNull
    @Enumerated(EnumType.STRING)
    private DroneStateStatus state;
    @DateCreated
    private LocalDateTime created;
    @DateUpdated
    private LocalDateTime updated;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name="DRONE_ID", referencedColumnName="ID", nullable = false)
    @Nullable
    private Set<Medication> medications;
}

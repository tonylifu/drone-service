package com.anthony.lifu.model.entity;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.data.annotation.DateCreated;
import io.micronaut.data.annotation.DateUpdated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @NonNull
    @Pattern(regexp = "([A-Za-z0-9\\-\\_]+)",
            message = "only accepts letters, numbers, dash and underscore")
    private String name;
    @NonNull
    private double weight;
    @NonNull
    @Pattern(regexp = "(?=[0-9_]*[A-Z])\\b[A-Z0-9_]+\\b",
            message = "accepts only UPPERCASE letters, underscore and numbers")
    private String code;
    @NonNull
    private String image;
    @DateCreated
    private LocalDateTime created;
    @DateUpdated
    private LocalDateTime updated;
    @Column(name = "DRONE_ID", insertable = false, updatable = false)
    private Long droneId;
}

## Drone Service

- Gitlab https://gitlab.com/tonylifu/drone-service/-/tree/main
- https://gitlab.com/tonylifu/drone-service.git
---
## Introduction
The Drone Service provides an api to manage a fleet of drones delivering medical supplies to locations not easy to reach or otherwise - revolutionizes logistics. 

The service is designed to be scalable, extensible and maintainable. They are three modules that make up the Drone Service:
- drone-api: plain old java that defines the contract and commons that is implemented all across the application.
- drone-model: a micronaut module that implements the drone-api and provides persistence.
- src: the root project that included the drone-api and the drone-model provides the restful endpoints for interacting with the application using the drone-api contract; and thereby fully agnostic of the persistence details.

More so, at application startup 10 drones detailed here are preloaded (registered) and are all in IDLE state. When loaded with medication and there is still allowance for more loading, the state changes to LOADING. Once the maximum capacity is reached and you are trying to load more, the state changes to LOADED and no further loading is allowed. Similarly, if the battery level is below 25% no loading is allowed.  
- see seeded data at application startup: [
  {
  "serialNumber": "abcdef-001",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 100,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-002",
  "model": "HEAVYWEIGHT",
  "weight": 100.0,
  "battery": 95,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-003",
  "model": "LIGHTWEIGHT",
  "weight": 90.0,
  "battery": 65,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-004",
  "model": "MIDDLEWEIGHT",
  "weight": 95.0,
  "battery": 99,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-005",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 85,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-006",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 75,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-007",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 20,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-008",
  "model": "LIGHTWEIGHT",
  "weight": 90.0,
  "battery": 100,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-009",
  "model": "CRUISERWEIGHT",
  "weight": 120.0,
  "battery": 100,
  "state": "IDLE"
  },
  {
  "serialNumber": "abcdef-010",
  "model": "CRUISERWEIGHT",
  "weight": 500.0,
  "battery": 100,
  "state": "IDLE"
  }
  ]

###Technology
- Micronaut 3.2.2, Java 17, Git, Gitlab, Spock
- Intellij IDEA, Postman
- In-Memory H2 Database

####API End Points
####BASE_URL http://localhost:8080/api/drone-service/v1/
The following endpoints are provided.
####1. register a drone: BASE_URL/register-drone
- Request:
  {
  "serialNumber": "abcdef-2022",
  "model": "HEAVYWEIGHT",
  "weight": 20,
  "battery": 75,
  "state": "IDLE"
  }
- Response:
  {
  "serialNumber": "abcdef-2022",
  "statusCode": "00",
  "statusMessage": "Successful",
  "success": true
  }
####2. Load Medication: BASE_URL/load-drone/medication/{droneSerialNo}
- Request: {
  "name": "PARACETAMOL-01",
  "weight": 5,
  "code": "MEDIC_01",
  "image": "iio89en44893409mffmndf899"
  }
- Response: {
  "serialNumber": "abcdef-001",
  "statusCode": "00",
  "statusMessage": "Successful",
  "success": true
  }
####3. Get Medications By Drone: BASE_URL/check-medications/{droneSerialNo}
- Response [as loaded]: {
  "medications": [
  {
  "name": "PARACETAMOL-02",
  "weight": 5.0,
  "code": "MEDIC_02",
  "image": "iio89en44893409mffmndf899"
  },
  {
  "name": "PARACETAMOL-01",
  "weight": 5.0,
  "code": "MEDIC_01",
  "image": "iio89en44893409mffmndf899"
  }
  ]
  }
####4. Get Available Drones for Loading [returns drones in IDLE and LOADING states]: BASE_URL/available-drones
- Response: [
  {
  "serialNumber": "abcdef-001",
  "model": "CRUISERWEIGHT",
  "weight": 125.0,
  "battery": 100,
  "state": "LOADING"
  },
  {
  "serialNumber": "abcdef-002",
  "model": "HEAVYWEIGHT",
  "weight": 100.0,
  "battery": 95,
  "state": "IDLE"
  },
...
####5. Check Drone Battery Level: BASE_URL/check-battery-level/{droneSerialNo}
- Response: {
  "serialNumber": "abcdef-001",
  "batteryLevel": "100.0%"
  }

####6. Job runs every 30 seconds and logs battery level and other details to a log file /logs/log.log
There are plenty rooms for improvement and adding more functionalities to the project.


package com.anthony.lifu.drone.service.controller;

import com.anthony.lifu.api.constants.AppConstants;
import com.anthony.lifu.api.dto.request.AddMedicationRequest;
import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.api.enums.ErrorMsg;
import com.anthony.lifu.api.model.DroneModel;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import lombok.RequiredArgsConstructor;

@Controller("/api/drone-service/v1")
@RequiredArgsConstructor
public class DispatchController {
    private final DroneModel droneModel;

    @Post(value = "/register-drone", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public HttpResponse<?> registerDrone(@Body final RegisterDroneRequest droneRequest){
        return HttpResponse.created(droneModel.registerDrone(droneRequest));
    }

    @Put(value = "/load-drone/medication/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public HttpResponse<?> loadMedication(@Body final AddMedicationRequest medicationRequest, String droneSerialNumber){
        return HttpResponse.ok(droneModel.loadMedication(medicationRequest, droneSerialNumber));
    }

    @Get(value = "/check-medications/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<?> getLoadedMedications(String droneSerialNumber){
        var response = droneModel.getLoadedMedications(droneSerialNumber);
        return HttpResponse.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @Get(value = "/available-drones", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<?> getAvailableDrones(){
        var response = droneModel.getAvailableDrones();
        return HttpResponse.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @Get(value = "/check-battery-level/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<?> checkBatteryLevel(String droneSerialNumber){
        var response = droneModel.droneBatteryLevel(droneSerialNumber);
        return HttpResponse.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @Get(value = "/drones-by-state/{droneState}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<?> getDronesByState(String droneState){
        DroneStateStatus state = DroneStateStatus.UNKNOWN;
        try {
            state = DroneStateStatus.valueOf(droneState);
        } catch (Exception e) {
            return HttpResponse.badRequest(AppConstants.apiResponse(Boolean.FALSE, null,
                    ErrorMsg.DRONE_STATE_NOT_FOUND.getCode(), ErrorMsg.DRONE_STATE_NOT_FOUND.getDescription()));
        }
        var response = droneModel.getDronesByState(state);
        return HttpResponse.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }

    @Get(value = "/all-drones", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<?> getAllDrones(){
        var response = droneModel.getAllDrones();
        return HttpResponse.ok(response
                .fold(
                        lr -> response.getLeft(), rr -> response.get()
                ));
    }
}

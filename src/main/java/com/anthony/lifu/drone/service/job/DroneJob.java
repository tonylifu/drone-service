package com.anthony.lifu.drone.service.job;

import com.anthony.lifu.api.model.DroneModel;
import io.micronaut.scheduling.annotation.Scheduled;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;

@Singleton
@RequiredArgsConstructor
public class DroneJob {
    private final DroneModel droneModel;

    @Scheduled(cron = "${drone.scheduler.cronexpression}")
    void checkBatteryLevel(){
        droneModel.dronesBatteryChecker();
    }
}

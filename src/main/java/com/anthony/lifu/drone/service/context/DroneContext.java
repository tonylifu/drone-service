package com.anthony.lifu.drone.service.context;

import com.anthony.lifu.api.model.DroneModel;
import com.anthony.lifu.model.repository.DroneRepository;
import com.anthony.lifu.model.service.DroneService;
import io.micronaut.context.annotation.Factory;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Factory
public class DroneContext {

    @Singleton
    public DroneModel droneModel(DroneRepository droneRepository){
        return new DroneService(droneRepository);
    }
}

package com.anthony.lifu.drone.service.config;

import com.anthony.lifu.api.dto.request.RegisterDroneRequest;
import com.anthony.lifu.api.enums.DroneModelStatus;
import com.anthony.lifu.api.enums.DroneStateStatus;
import com.anthony.lifu.api.model.DroneModel;
import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.runtime.server.event.ServerStartupEvent;
import jakarta.inject.Singleton;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

@Singleton
@RequiredArgsConstructor
public class DataLoader implements ApplicationEventListener<ServerStartupEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(DataLoader.class);
    private final DroneModel droneModel;

    @Override
    public void onApplicationEvent(ServerStartupEvent event) {
        LOG.info("\n\n:::Seeding Data -> Fleet of 10 Drones:::\n\n");
        getDrones()
                .stream()
                .map(dr -> droneModel.registerDrone(dr))
                .map(a -> a.get())
                .forEach(apiResponse -> LOG.info("\nisSuccess: {}, serialNo: {}, code: {}, message: {}",
                        apiResponse.isSuccess(), apiResponse.getSerialNumber(), apiResponse.getStatusCode(), apiResponse.getStatusMessage()));
    }

    private List<RegisterDroneRequest> getDrones() {
        return List.of(
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-001")
                        .battery(100)
                        .weight(120)
                        .model(DroneModelStatus.CRUISERWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-002")
                        .battery(95)
                        .weight(100)
                        .model(DroneModelStatus.HEAVYWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-003")
                        .battery(65)
                        .weight(90)
                        .model(DroneModelStatus.LIGHTWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-004")
                        .battery(99)
                        .weight(95)
                        .model(DroneModelStatus.MIDDLEWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-005")
                        .battery(85)
                        .weight(120)
                        .model(DroneModelStatus.CRUISERWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-006")
                        .battery(75)
                        .weight(120)
                        .model(DroneModelStatus.CRUISERWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-007")
                        .battery(20)
                        .weight(120)
                        .model(DroneModelStatus.CRUISERWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-008")
                        .battery(100)
                        .weight(90)
                        .model(DroneModelStatus.LIGHTWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-009")
                        .battery(100)
                        .weight(120)
                        .model(DroneModelStatus.CRUISERWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build(),
                RegisterDroneRequest.builder()
                        .serialNumber("abcdef-010")
                        .battery(100)
                        .weight(500)
                        .model(DroneModelStatus.CRUISERWEIGHT.name())
                        .state(DroneStateStatus.IDLE.name()).build()
        );

    }
}
